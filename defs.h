/*********************************************************************
	Define constants and macros
 *********************************************************************/

#define ELEMq	1.60217662e-19	// elementary charge [C]

#define SIZE_FILENAMES	50	// max. no. of characters allowed in a filename 
