# ***********************************
#	Make file
# ***********************************

# Compiler location
CC = /usr/local/bin/gcc

# Compiler options
CFLAGS = -I . -O3
#CFLAGS = -g # for debugging purposes

all: main.o simlcases.o init.o pic.o move.o grid.o particle.o index.o interpolate.o fields.o sample.o outputformats.o files.o
	$(CC) main.o simlcases.o init.o pic.o move.o grid.o particle.o index.o interpolate.o fields.o sample.o outputformats.o files.o -o pic_1

main.o: main.h simlcases.h main.c
	$(CC) $(CFLAGS) -c main.c

simlcases.o: main.h init.h pic.h files.h simlcases.c
	$(CC) $(CFLAGS) -c simlcases.c

init.o: main.h grid.h particle.h init.c
	$(CC) $(CFLAGS) -c init.c

pic.o: main.h move.h grid.h interpolate.h fields.h sample.h pic.c
	$(CC) $(CFLAGS) -c pic.c

move.o: main.h move.h move.c
	$(CC) $(CFLAGS) -c move.c

grid.o: main.h grid.h interpolate.h index.h fields.h grid.c
	$(CC) $(CFLAGS) -c grid.c

particle.o: main.h particle.c
	$(CC) $(CFLAGS) -c particle.c

index.o: main.h index.c
	$(CC) $(CFLAGS) -c index.c

interpolate.o: main.h interpolate.h index.h interpolate.c
	$(CC) $(CFLAGS) -c interpolate.c

fields.o: main.h fields.h fields.c
	$(CC) $(CFLAGS) -c fields.c

sample.o: main.h sample.h outputformats.h sample.c
	$(CC) $(CFLAGS) -c sample.c

outputformats.o: main.h outputformats.c
	$(CC) $(CFLAGS) -c outputformats.c

files.o: main.h files.c
	$(CC) $(CFLAGS) -c files.c

clean:
	rm -rf *.o
	rm -i pic_1
