/*********************************************************************
	Define structures
 *********************************************************************/

/* Particles */
struct Particle
{
  double pos[3]; // position [m]
  double vel[3]; // velocity [m/s]
  double mom;	 // momentum [kg-m/s]
  double KE;	 // kinetic energy [J]
} ;

/* Grid */
struct Grid
{
  double pos[3];    // position [m]
  double qdens;     // charge density [C/m^3]
  double phi;       // electrostatic potential [V]
  double Efield[3]; // electric field [V/m]
  double ESE; 	    // electrostatic field (potential) energy [J]
} ;

/* Species */
struct Species
{
  int Np;	     // no. of computational particles
  double mass;	     // particle mass of species [kg]
  int q;	     // particle charge of species [unit elementary charge]
  double qdtoverm;   // charge x dt/mass [C-s/kg]
  double fnum;	     // no. of real particles per computational particle
  double qoverVcell; // particle charge of species over cell volume [C/m^3] (only used when grid is uniform in each direction)
} ;

/* Simulation parameters */
struct SimlParameters
{
  double dt;	  // timestep size [s]
  int Ndt;	  // no. of timesteps
  int Nsp;	  // no. of species
  int Ngrid[3];   // no. of grid points in each direction
  double pos0[3]; // starting point of grid in each direction [m]
  double Ldom[3]; // dimension of domain in each direction [m]
  double dx[3];   // grid spacing in each direction [m] (only used when grid is uniform in each direction)
  double Vcell;   // cell volume [m^3] (only used when grid is uniform in each direction)
} ;

/* Sampling parameters */
struct SamplParameters
{
  int start;		// timestep to begin sampling
  int Ndtbtwsamples;	// no. of timesteps between samples
} ;

/* Files and filenames */
struct Files
{
  /* [OUTPUT] File outputting particle properties */
  char filename_outparticleprops[SIZE_FILENAMES];
  FILE* file_outparticleprops;

  /* [OUTPUT] File outputting grid properties */
  char filename_outgridprops[SIZE_FILENAMES];
  FILE* file_outgridprops;
} ;
