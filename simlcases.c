
#include "main.h"
#include "init.h"
#include "pic.h"
#include "files.h"

void RunSimlGeneral(void)
/**********************************************************************

	< Describe function. >

	INPUTS:
	OUTPUTS:

 **********************************************************************/
{
  int is; // loop variables

  /* ======== Declare variables ======== */

  /* Data structures */
  struct SimlParameters simlparameters;
  struct SamplParameters samplparameters;
  struct Species* species = NULL;
  struct Grid* grid = NULL;
  struct Particle** particle = NULL;

  /* Files and filenames */
  char* filename_base = "runsimlgeneral"; // base filename

  struct Files filesRunSimlGeneral;

  /* ======== Initialize simulation ======== */

  /* Get simulation parameters */
  GetSimlParameters(&simlparameters);

  /* Get sampling parameters */
  GetSamplParameters(&samplparameters);

  /* Allocate memory for species data structure */
  species = (struct Species*) calloc(simlparameters.Nsp, sizeof(struct Species));
  if (!species) { 
    printf("Memory allocation failed! Species data structure is not assigned. Exiting.\n");
    exit(1);
  }

  /* Allocate memory for grid data structure */
  grid = (struct Grid*) calloc(simlparameters.Ngrid[0]*simlparameters.Ngrid[1]*simlparameters.Ngrid[2], sizeof(struct Grid));
  if (!grid) {
    printf("Memory allocation failed! Grid data structure is not assigned. Exiting.\n");
    exit(1);
  }

  /* Get species properties */
  GetSpeciesProps(simlparameters, species);

  /* Allocate memory for particle data structure */
  particle = (struct Particle**) calloc(simlparameters.Nsp, sizeof(struct Particle*));

  for (is = 0; is < simlparameters.Nsp; is++) {
    particle[is] = (struct Particle*) calloc(species[is].Np, sizeof(struct Particle));
    if (!particle[is]) {
      printf("Memory allocation failed! Particle data structure for species %d is not assigned. Exiting.\n", is);
      exit(1);
    }
  }

  /* Open files */
  OpenFiles(&filesRunSimlGeneral, filename_base);

  /* Initialize particle positions and velocities */
  InitializeParticlePos(simlparameters, species, grid, particle);
  InitializeParticleVel(simlparameters, species, particle);

  /* Initialize grid */
  InitializeGrid(simlparameters, species, grid, particle);

  /* ======== Execute PIC routine ======== */

  ExecutePIC(simlparameters, samplparameters, species, grid, particle, &filesRunSimlGeneral);

  /* ======== Clean up program ======== */

  /* Free memory */
  free(species);
  free(grid); 
  for (is = 0; is < simlparameters.Nsp; is++) { 
    free(particle[is]);
  }
  free(particle);

  /* Close files */
  CloseFiles(&filesRunSimlGeneral);

  printf("\n******** Simulation is complete. ********\n"); 

  return;
} // end RunSimlGeneral()
