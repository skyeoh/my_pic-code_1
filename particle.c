
#include "main.h"

void SetParticlePosSamePosInCell(struct SimlParameters simlparameters,
				 struct Species* species, struct Grid* grid,
				 struct Particle** particle)
/**********************************************************************

        Place particles at the same locations within the cell
	in each cell.

	Note that this only works for a grid that is uniform
	in each direction.

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  int i, is, ipart; 		// loop variables
  int Nsp = simlparameters.Nsp;	// no. of species
  int Ncell[3], Ncelltotal;

  double pos0[3], dx[3];
  double frac0[Nsp], frac1[Nsp], frac2[Nsp];

  struct Particle* particlePtr;

  /* Assign and calculate variables */
  for (i = 0; i < 3; i++) {

    Ncell[i] = simlparameters.Ngrid[i] - 1; 
    pos0[i] = simlparameters.pos0[i];
    dx[i] = simlparameters.dx[i];

  }
  
  Ncelltotal = Ncell[0]*Ncell[1]*Ncell[2]; // total no. of cells

  for (is = 0; is < Nsp; is++) {

    /* Set default particle position at cell center */
    frac0[is] = 0.5;
    frac1[is] = 0.5;
    frac2[is] = 0.5; 
    
    /* Check that there are enough particles to populate all the cells */
    if ((species[is].Np%Ncelltotal) != 0) {
      printf("Pick a different total no. of particles for species %d so that there is enough particles to populate all the cells. Exiting.\n", is); 
      exit(1);
    }
  
  } // end loop over species

  for (is = 0; is < Nsp; is++) {
    for (ipart = 0; ipart < species[is].Np; ipart++) {
      
      particlePtr = particle[is] + ipart; // increment particle pointer

      particlePtr->pos[0] = pos0[0] + (frac0[is] + ipart%Ncell[0])*dx[0];
      particlePtr->pos[1] = pos0[1] + (frac1[is] + (ipart/Ncell[0])%Ncell[1])*dx[1];
      particlePtr->pos[2] = pos0[2] + (frac2[is] + (ipart/(Ncell[0]*Ncell[1]))%Ncell[2])*dx[2];

    } // end loop over particles
  } // end loop over species

  return;
} // end SetParticlePosSamePosInCell()
