
void InterpolateParticlePropsToGrid(struct SimlParameters simlparameters, struct Species* species,
                                    struct Grid* grid, struct Particle* particle, int is);
void InterpolateGridPropsToParticles(struct SimlParameters simlparameters, struct Grid* grid,
                                     struct Particle* particle, double* Efield);
void InterpolatePGLinearWtsUniformGrid(struct SimlParameters simlparameters, struct Species* species,
                                       struct Grid* grid, struct Particle* particle, int is);
void InterpolateGPLinearWtsUniformGrid(struct SimlParameters simlparameters, struct Grid* grid,
                                       struct Particle* particle, double* Efield);
double CalculateLinearWtsUniformGrid(struct Particle* particle, struct Grid* grid, double* dx);
