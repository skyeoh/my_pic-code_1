
#include "main.h"
#include "sample.h"
#include "outputformats.h"

void SampleProps(int it, double time, 
		 struct Files* files,
		 struct SimlParameters simlparameters, 
		 struct SamplParameters samplparameters,
		 struct Species* species, struct Grid* grid, 
		 struct Particle** particle)
/**********************************************************************

        Sample and output properties. < Be more descriptive!>

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  OutputParticleProps(it, time, files, simlparameters, samplparameters, species, particle);

  OutputGridProps(it, time, files, simlparameters, samplparameters, grid);

  return;
} // end SampleProps()

void OutputParticleProps(int it, double time,
                 	 struct Files* files,
                 	 struct SimlParameters simlparameters,
                 	 struct SamplParameters samplparameters,
                 	 struct Species* species, struct Particle** particle)
/**********************************************************************

        Sample and output particle properties. < Be more descriptive!>

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  /* Set up output file header */
  if (it == samplparameters.start) {

    fprintf(files->file_outparticleprops, "TITLE = \"Particle properties\"\n");
    fprintf(files->file_outparticleprops, "VARIABLES = \"Time [s]\", \"Position time [s]\", \"Velocity time [s]\"");
    fprintf(files->file_outparticleprops, ", \"x [m]\", \"y [m]\", \"z [m]\"");
    fprintf(files->file_outparticleprops, ", \"u [m/s]\", \"v [m/s]\", \"w [m/s]\"");
    fprintf(files->file_outparticleprops, ", \"Momentum [kg-m/s]\", \"KE [J]\"");
    fprintf(files->file_outparticleprops, ", \"Species\"\n");

  }

  /* Check if it is time to sample */
  if ( (it >= samplparameters.start) && 
       (((it-samplparameters.start)%samplparameters.Ndtbtwsamples) == 0) ) { 

    OutputParticlePropsTecplot(it, time, files, simlparameters, samplparameters, species, particle);  

  }

  return;
} // end OutputParticleProps()

void OutputGridProps(int it, double time,
                     struct Files* files,
                     struct SimlParameters simlparameters,
                     struct SamplParameters samplparameters,
                     struct Grid* grid)
/**********************************************************************

        Sample and output grid properties. < Be more descriptive!>

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  /* Set up output file header */
  if (it == samplparameters.start) {

    fprintf(files->file_outgridprops, "TITLE = \"Grid properties\"\n");
    fprintf(files->file_outgridprops, "VARIABLES = \"Time [s]\"");
    fprintf(files->file_outgridprops, ", \"x [m]\", \"y [m]\", \"z [m]\"");
    fprintf(files->file_outgridprops, ", \"Charge dens. [C/m^3]\", \"E-S potential [V]\"");
    fprintf(files->file_outgridprops, ", \"Ex [V/m]\", \"Ey [V/m]\", \"Ez [V/m]\"");
    fprintf(files->file_outgridprops, ", \"E-S potential energy [J]\"\n");

  }

  /* Check if it is time to sample */
  if ( (it >= samplparameters.start) &&
       (((it-samplparameters.start)%samplparameters.Ndtbtwsamples) == 0) ) {

    OutputGridPropsTecplot(it, time, files, simlparameters, samplparameters, grid);

  }

  return;
} // end OutputGridProps()
