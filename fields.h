
void EvaluateFields(struct Particle* particle, double time, double* Efield);
void CalculateEField(struct Particle* particle, double time, double* Efield);
void AssignEFieldToGrid(struct SimlParameters simlparameters, struct Grid* grid);
