
#include "main.h"
#include "interpolate.h"
#include "index.h"

void InterpolateParticlePropsToGrid(struct SimlParameters simlparameters, struct Species* species,
				    struct Grid* grid, struct Particle* particle, int is)
/**********************************************************************

        Interpolate the properties of the particles to the grid.
	< Be more descriptive! >

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  InterpolatePGLinearWtsUniformGrid(simlparameters, species, grid, particle, is);

  /*
   * Include other schemes as necessary
   */

  return;
} // end InterpolateParticlePropsToGrid()

void InterpolateGridPropsToParticles(struct SimlParameters simlparameters, struct Grid* grid,
                                     struct Particle* particle, double* Efield)
/**********************************************************************

        Interpolate the properties on the grid to the particles. 
	< Be more descriptive! >

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  InterpolateGPLinearWtsUniformGrid(simlparameters, grid, particle, Efield);

  /*
   * Include other schemes as necessary
   */

  return;
} // end InterpolateGridPropsToParticles()

void InterpolatePGLinearWtsUniformGrid(struct SimlParameters simlparameters, struct Species* species,
				       struct Grid* grid, struct Particle* particle, int is)
/**********************************************************************

        Perform linear interpolation of particle properties 
	to a uniform Cartesian grid. < Be more descriptive! >

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  int i, ic[3], gridindex[8];
  int Ngrid0, Ngrid0Ngrid1;
  double gridweight[8], dx[3];
  double fnum;

  /* Assign variables */
  Ngrid0 = simlparameters.Ngrid[0];
  Ngrid0Ngrid1 = Ngrid0*simlparameters.Ngrid[1];

  dx[0] = simlparameters.dx[0];
  dx[1] = simlparameters.dx[1];
  dx[2] = simlparameters.dx[2];

  fnum = species[is].fnum;

  /* Get particle cell indices */
  FindParticleCellIndexUniformGrid(simlparameters, particle, ic);

  /* Get indices of adjacent grid points */
  gridindex[0] = FindGridIndex(ic[0],   ic[1], 	 ic[2],   Ngrid0, Ngrid0Ngrid1);
  gridindex[1] = FindGridIndex(ic[0]+1, ic[1], 	 ic[2],   Ngrid0, Ngrid0Ngrid1);
  gridindex[2] = FindGridIndex(ic[0],   ic[1]+1, ic[2],   Ngrid0, Ngrid0Ngrid1);
  gridindex[3] = FindGridIndex(ic[0]+1, ic[1]+1, ic[2],   Ngrid0, Ngrid0Ngrid1);
  gridindex[4] = FindGridIndex(ic[0], 	ic[1], 	 ic[2]+1, Ngrid0, Ngrid0Ngrid1);
  gridindex[5] = FindGridIndex(ic[0]+1, ic[1], 	 ic[2]+1, Ngrid0, Ngrid0Ngrid1);
  gridindex[6] = FindGridIndex(ic[0], 	ic[1]+1, ic[2]+1, Ngrid0, Ngrid0Ngrid1);
  gridindex[7] = FindGridIndex(ic[0]+1, ic[1]+1, ic[2]+1, Ngrid0, Ngrid0Ngrid1);

  /* Calculate weights and charge densities for each grid point */ 
  for (i = 0; i < 8; i++) {
    gridweight[i] = CalculateLinearWtsUniformGrid(particle, grid+gridindex[i], dx);
    grid[gridindex[i]].qdens += gridweight[i]*fnum*species[is].qoverVcell;
  }

  return;
} // end InterpolatePGLinearWtsUniformGrid()

void InterpolateGPLinearWtsUniformGrid(struct SimlParameters simlparameters, struct Grid* grid,
                                       struct Particle* particle, double* Efield)
/**********************************************************************

        Perform linear interpolation of properties on a 
	uniform Cartesian grid to the particles. < Be more descriptive! >

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  int i, ic[3], gridindex[8];
  int Ngrid0, Ngrid0Ngrid1;
  double gridweight[8], dx[3];

  /* Assign variables */
  Ngrid0 = simlparameters.Ngrid[0];
  Ngrid0Ngrid1 = Ngrid0*simlparameters.Ngrid[1];

  dx[0] = simlparameters.dx[0];
  dx[1] = simlparameters.dx[1];
  dx[2] = simlparameters.dx[2];

  /* Get particle cell indices */
  FindParticleCellIndexUniformGrid(simlparameters, particle, ic);

  /* Get indices of adjacent grid points */
  gridindex[0] = FindGridIndex(ic[0],   ic[1], 	 ic[2],   Ngrid0, Ngrid0Ngrid1);
  gridindex[1] = FindGridIndex(ic[0]+1, ic[1], 	 ic[2],   Ngrid0, Ngrid0Ngrid1);
  gridindex[2] = FindGridIndex(ic[0],   ic[1]+1, ic[2],   Ngrid0, Ngrid0Ngrid1);
  gridindex[3] = FindGridIndex(ic[0]+1, ic[1]+1, ic[2],   Ngrid0, Ngrid0Ngrid1);
  gridindex[4] = FindGridIndex(ic[0], 	ic[1], 	 ic[2]+1, Ngrid0, Ngrid0Ngrid1);
  gridindex[5] = FindGridIndex(ic[0]+1, ic[1], 	 ic[2]+1, Ngrid0, Ngrid0Ngrid1);
  gridindex[6] = FindGridIndex(ic[0], 	ic[1]+1, ic[2]+1, Ngrid0, Ngrid0Ngrid1);
  gridindex[7] = FindGridIndex(ic[0]+1, ic[1]+1, ic[2]+1, Ngrid0, Ngrid0Ngrid1);

  /* Initialize sums to zero */
  Efield[0] = 0.0;
  Efield[1] = 0.0;
  Efield[2] = 0.0;

  /* Calculate electric field on the particle */
  for (i = 0; i < 8; i++) {
    gridweight[i] = CalculateLinearWtsUniformGrid(particle, grid+gridindex[i], dx);

    Efield[0] += gridweight[i]*grid[gridindex[i]].Efield[0];
    Efield[1] += gridweight[i]*grid[gridindex[i]].Efield[1];
    Efield[2] += gridweight[i]*grid[gridindex[i]].Efield[2];
  }

  return;
} // end InterpolateGPLinearWtsUniformGrid()

double CalculateLinearWtsUniformGrid(struct Particle* particle, struct Grid* grid, double* dx)
/**********************************************************************
	
	Calculate the weights for a 3D linear interpolation
	to/from a uniform Cartesian grid. < Be more descriptive! >

 **********************************************************************/
{
  double weight[3];
  double totalweight;

  weight[0] = 1 - fabs(particle->pos[0] - grid->pos[0])/dx[0];
  weight[1] = 1 - fabs(particle->pos[1] - grid->pos[1])/dx[1];
  weight[2] = 1 - fabs(particle->pos[2] - grid->pos[2])/dx[2];

  totalweight = weight[0]*weight[1]*weight[2];

  return totalweight;
} // end CalculateLinearWtsUniformGrid()
