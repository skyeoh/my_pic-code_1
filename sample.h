
void SampleProps(int it, double time, 
                 struct Files* files,
                 struct SimlParameters simlparameters,
                 struct SamplParameters samplparameters,
                 struct Species* species, struct Grid* grid,
		 struct Particle** particle);
void OutputParticleProps(int it, double time,
                         struct Files* files,
                         struct SimlParameters simlparameters,
                         struct SamplParameters samplparameters,
                         struct Species* species, struct Particle** particle);
void OutputGridProps(int it, double time,
                     struct Files* files,
                     struct SimlParameters simlparameters,
                     struct SamplParameters samplparameters,
                     struct Grid* grid);
