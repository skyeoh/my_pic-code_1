
#include "main.h"
#include "grid.h"
#include "particle.h"

void GetSimlParameters(struct SimlParameters* simlparameters)
/**********************************************************************

	Get simulation parameters. <Be more descriptive!>

	INPUTS:
	OUTPUTS:

 **********************************************************************/
{
  /* Set simulation parameters */
  simlparameters->dt = 0.01;	// timestep size [s]
  simlparameters->Ndt = 100;	// no. of timesteps
  simlparameters->Nsp = 1;	// no. of species

  simlparameters->Ngrid[0] = 8; // no. of grid points in each dir.
  simlparameters->Ngrid[1] = 8;
  simlparameters->Ngrid[2] = 8;

  simlparameters->pos0[0] = -0.5; // starting point of grid in each dir. [m]
  simlparameters->pos0[1] = -0.5;
  simlparameters->pos0[2] = -0.5;

  simlparameters->Ldom[0] = 1.0; // dimension of domain in each dir. [m]
  simlparameters->Ldom[1] = 1.0;
  simlparameters->Ldom[2] = 1.0;

  simlparameters->dx[0] = simlparameters->Ldom[0]/(simlparameters->Ngrid[0]-1); // grid spacing in each dir. [m] (only used when grid is uniform in each dir.)
  simlparameters->dx[1] = simlparameters->Ldom[1]/(simlparameters->Ngrid[1]-1);
  simlparameters->dx[2] = simlparameters->Ldom[2]/(simlparameters->Ngrid[2]-1);

  simlparameters->Vcell = (simlparameters->dx[0])*(simlparameters->dx[1])*(simlparameters->dx[2]); // cell volume [m^3] (only used when grid is uniform in each dir.)

  return;
} // end GetSimlParameters()

void GetSamplParameters(struct SamplParameters* samplparameters)
/**********************************************************************

	Get sampling parameters. <Be more descriptive!>

	INPUTS:
	OUTPUTS:

 **********************************************************************/
{
  /* Set sampling parameters */
  samplparameters->start = 0;
  samplparameters->Ndtbtwsamples = 10;

  return;
} // end GetSamplParameters()

void GetSpeciesProps(struct SimlParameters simlparameters, struct Species* species)
/**********************************************************************

	Get species properties. <Be more descriptive!>

	INPUTS:
	OUTPUTS:

 **********************************************************************/
{
  int is;

  /*
   * WARNING: For now, function only works for ONE species.
   * 	      It will eventually read from a species input file.   
   */

  /* Get species properties */
  for (is = 0; is < simlparameters.Nsp; is++) {
    species[is].Np = 1; 		// no. of computational particles
    species[is].mass = 9.10938356e-31; 	// particle mass [kg]
    species[is].q = -1; 		// particle charge [unit elementary charge]
    species[is].fnum = 1e3;		// no. of real particles per computational particle

    /* Calculate charge x dt/mass [C-s/kg] */
    species[is].qdtoverm = (species[is].q*ELEMq)*(simlparameters.dt)/(species[is].mass);

    /* Calculate particle charge over cell volume [C/m^3] */
    species[is].qoverVcell = (species[is].q*ELEMq)/(simlparameters.Vcell);
  }

  return;
} // end GetSpeciesProps()

void InitializeParticlePos(struct SimlParameters simlparameters,
			   struct Species* species, struct Grid* grid,
			   struct Particle** particle)
/**********************************************************************

	Initialize particle positions. <Be more descriptive!>

	INPUTS:
	OUTPUTS:

 **********************************************************************/
{
  SetParticlePosSamePosInCell(simlparameters, species, grid, particle);  

  /* 
   * Include other functions to initialize particle positions.
   * There are two ways to initialize particle positions:
   *   (1) using functions for typical distributions
   *   (2) using a function that will read from a file for more general distributions   
   */

  return;
} // end InitializeParticlePos()

void InitializeParticleVel(struct SimlParameters simlparameters,
			   struct Species* species, struct Particle** particle)
/**********************************************************************

	Initialize particle velocities. <Be more descriptive!>

	INPUTS:
	OUTPUTS:

 **********************************************************************/
{
  int is, ipart; // loop variables

  /*
   * WARNING: Same one as in InitializeParticlePos().
   */

  for (is = 0; is < simlparameters.Nsp; is++) {
    for (ipart = 0; ipart < species[is].Np; ipart++) {

      (particle[is]+ipart)->vel[0] = 0.0;
      (particle[is]+ipart)->vel[1] = 0.0;
      (particle[is]+ipart)->vel[2] = 0.0;

    } // loop over particles
  } // loop over species

  return;
} // end InitializeParticleVel()

void InitializeGrid(struct SimlParameters simlparameters,
		    struct Species* species, struct Grid* grid,
		    struct Particle** particle)
/**********************************************************************

        Initialize the positions of the grid points and the properties
        on the grid, e.g. charge density. <Be more descriptive!>

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  SetGridPos(simlparameters, grid);
  SetGridProps(simlparameters, species, grid, particle);

  return;
} // end InitializeGrid()
