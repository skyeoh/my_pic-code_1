
void OutputParticlePropsTecplot(int it, double time,
                                struct Files* files,
                                struct SimlParameters simlparameters,
                                struct SamplParameters samplparameters,
                                struct Species* species, struct Particle** particle);
void OutputGridPropsTecplot(int it, double time,
                            struct Files* files,
                            struct SimlParameters simlparameters,
                            struct SamplParameters samplparameters,
                            struct Grid* grid);
