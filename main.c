/**********************************************************************

	<Include general remarks and description of code, features, etc.>

 **********************************************************************/

#include "main.h"
#include "simlcases.h"

int main(int argc, char* argv[])
/**********************************************************************

	Main function.

	INPUTS: None.
	OUTPUTS: None.

 **********************************************************************/
{
  int i, j, k; // loop variables

  RunSimlGeneral();

  return 0;
} // end main()
