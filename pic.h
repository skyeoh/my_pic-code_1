
void ExecutePIC(struct SimlParameters simlparameters,
		struct SamplParameters samplparameters, 
                struct Species* species, struct Grid* grid,
		struct Particle** particle,
		struct Files* files);
