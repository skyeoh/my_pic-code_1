
#include "main.h"
#include "move.h"
#include "grid.h"
#include "interpolate.h"
#include "fields.h"
#include "sample.h"

void ExecutePIC(struct SimlParameters simlparameters,
		struct SamplParameters samplparameters, 
		struct Species* species, struct Grid* grid, 
		struct Particle** particle,
		struct Files* files)
/**********************************************************************

	< Describe function. >

	INPUTS:
	OUTPUTS:

 **********************************************************************/
{
  int is, ipart, it; // loop variables

  double dt;    // timestep size [s]
  int Ndt;      // no. of timesteps
  int Nsp;      // no. of species

  double time; 	    // current time [s]
  double Efield[3]; // electric field [V/m]

  struct Particle* particlePtr;

  /* Reassign simulation parameters for convenience */
  dt = simlparameters.dt;
  Ndt = simlparameters.Ndt;
  Nsp = simlparameters.Nsp;

  /* Push particle velocities back in time by 1/2 dt */
  for (is = 0; is < Nsp; is++) {
    for (ipart = 0; ipart < species[is].Np; ipart++) {

      particlePtr = particle[is] + ipart; // increment particle pointer 

//      EvaluateFields(particlePtr, 0.0, Efield); // temporarily put in place of interpolate g->p

      InterpolateGridPropsToParticles(simlparameters, grid, particlePtr, Efield);
      UpdateParticleVel(particlePtr, species[is], -0.5*species[is].qdtoverm, Efield);

    } // end loop over particles
  } // end loop over species

  /* Loop over timesteps */
  for (it = 0; it < Ndt; it++) {

    /* Simulation time */
    time = it*dt;

    /* Set charge densities on the grid to zero */
    ZeroGridProps(simlparameters, grid);

    for (is = 0; is < Nsp; is++) {
      for (ipart = 0; ipart < species[is].Np; ipart++) {

        particlePtr = particle[is] + ipart; // increment particle pointer 

	InterpolateParticlePropsToGrid(simlparameters, species, grid, particlePtr, is);

      } // end loop over particles
    } // end loop over species

    AssignEFieldToGrid(simlparameters, grid); // temporarily put in place of E-field solver

    /*
     * < Solve for fields on grid >
     */

    for (is = 0; is < Nsp; is++) {
      for (ipart = 0; ipart < species[is].Np; ipart++) {

        particlePtr = particle[is] + ipart; // increment particle pointer

//	EvaluateFields(particlePtr, time, Efield); // temporarily put in place of interpolate g->p

	InterpolateGridPropsToParticles(simlparameters, grid, particlePtr, Efield);
	MoveParticles(particlePtr, species[is], Efield, dt);

     } // end loop over particles
    } // end loop over species

    /* Sample properties */
    SampleProps(it, time, files, simlparameters, samplparameters, species, grid, particle);

  } // end loop over timesteps

  return;
} // end ExecutePIC()
