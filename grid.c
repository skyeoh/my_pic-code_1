
#include "main.h"
#include "grid.h"
#include "interpolate.h"
#include "index.h"
#include "fields.h"

void SetGridPos(struct SimlParameters simlparameters, struct Grid* grid)
/**********************************************************************

        Set the positions of the grid points. <Be more descriptive!>

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  SetGridPosUniform(simlparameters, grid);

  /*
   * Include other distributions of grid points as necessary
   */

  return;
} // end SetGridPos()

void SetGridProps(struct SimlParameters simlparameters,
		  struct Species* species, struct Grid* grid,
		  struct Particle** particle)
/**********************************************************************

        Set the properties on the grid, e.g. charge density. <Be more descriptive!>

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  int is, ipart; // loop variables

  struct Particle* particlePtr;

  for (is = 0; is < simlparameters.Nsp; is++) {
    for (ipart = 0; ipart < species[is].Np; ipart++) {

      particlePtr = particle[is] + ipart; // increment particle pointer

      InterpolateParticlePropsToGrid(simlparameters, species, grid, particlePtr, is);

    } // end loop over particles
  } // end loop over species

  AssignEFieldToGrid(simlparameters, grid); // temporarily put in place of E-field solver

  /*
   * < Solve for fields on grid >
   */

  return;
} // end SetGridProps()

void SetGridPosUniform(struct SimlParameters simlparameters, struct Grid* grid)
/**********************************************************************

        Set the positions of the grid points of a 
	uniform Cartesian grid. <Be more descriptive!>

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  int ic0, ic1, ic2;
  int Ngrid[3], Ngrid0Ngrid1;
  int gridindex;

  /* Assign variables */
  Ngrid[0] = simlparameters.Ngrid[0];
  Ngrid[1] = simlparameters.Ngrid[1];
  Ngrid[2] = simlparameters.Ngrid[2];

  Ngrid0Ngrid1 = Ngrid[0]*Ngrid[1];

  for (ic2 = 0; ic2 < Ngrid[2]; ic2++) {
    for (ic1 = 0; ic1 < Ngrid[1]; ic1++) {
      for (ic0 = 0; ic0 < Ngrid[0]; ic0++) {

	gridindex = FindGridIndex(ic0, ic1, ic2, Ngrid[0], Ngrid0Ngrid1);
	
	grid[gridindex].pos[0] = simlparameters.pos0[0] + ic0*simlparameters.dx[0];
	grid[gridindex].pos[1] = simlparameters.pos0[1] + ic1*simlparameters.dx[1];
	grid[gridindex].pos[2] = simlparameters.pos0[2] + ic2*simlparameters.dx[2];
	
      } // end loop over ic0
    } // end loop over ic1
  } // end loop over ic2

  return;
} // end SetGridPosUniform()

void ZeroGridProps(struct SimlParameters simlparameters, struct Grid* grid)
/**********************************************************************

        Set the properties on the grid to zero.

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  int i, Ngrid0Ngrid1Ngrid2;
  
  Ngrid0Ngrid1Ngrid2 = simlparameters.Ngrid[0]*simlparameters.Ngrid[1]*simlparameters.Ngrid[2];

  for (i = 0; i < Ngrid0Ngrid1Ngrid2; i++) {
    grid[i].qdens = 0.0;
  }

  return;
} // end ZeroGridProps()
