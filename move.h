
void MoveParticles(struct Particle* particle, struct Species species, 
                   double* Efield, double dt);
void UpdateParticleVel(struct Particle* particle, struct Species species, 
		       double qdtoverm, double* Efield);
void UpdateParticlePos(struct Particle* particle, double dt);
