
#include "main.h"

void FindParticleCellIndexUniformGrid(struct SimlParameters simlparameters,
				      struct Particle* particle, int* ic)
/**********************************************************************

        Find the index of the cell in which the particle 
	is currently located on a uniform Cartesian grid. 
	< Add details? >

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  ic[0] = (int) floor((particle->pos[0] - simlparameters.pos0[0])/simlparameters.dx[0]);
  ic[1] = (int) floor((particle->pos[1] - simlparameters.pos0[1])/simlparameters.dx[1]);
  ic[2] = (int) floor((particle->pos[2] - simlparameters.pos0[2])/simlparameters.dx[2]);

  /* When particles are located on the RHS surfaces of the domain in each direction */
  if (ic[0] == (simlparameters.Ngrid[0]-1)) { ic[0] = ic[0] - 1; }
  if (ic[1] == (simlparameters.Ngrid[1]-1)) { ic[1] = ic[1] - 1; }
  if (ic[2] == (simlparameters.Ngrid[2]-1)) { ic[2] = ic[2] - 1; }

  return;
} // end FindParticleCellIndexUniformGrid()

int FindGridIndex(int ic0, int ic1, int ic2, int Ngrid0, int Ngrid0Ngrid1)
/**********************************************************************

        Find the grid index. < Add details? >

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  int gridindex;

  gridindex = ic0 + ic1*Ngrid0 + ic2*Ngrid0Ngrid1;

  return gridindex;
} // end FindGridIndex()
