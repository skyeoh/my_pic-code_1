
void SetGridPos(struct SimlParameters simlparameters, struct Grid* grid);
void SetGridProps(struct SimlParameters simlparameters,
		  struct Species* species, struct Grid* grid,
		  struct Particle** particle);
void SetGridPosUniform(struct SimlParameters simlparameters, struct Grid* grid);
void ZeroGridProps(struct SimlParameters simlparameters, struct Grid* grid);
