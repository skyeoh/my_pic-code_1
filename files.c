
#include "main.h"

void OpenFiles(struct Files* files, char* filename_base)
/**********************************************************************

        Open files and assign their names.
	Always use with CloseFiles() to ensure that 
	files are properly closed after usage.

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  /* [OUTPUT] File outputting particle properties */
  sprintf(files->filename_outparticleprops, "%s_outparticleprops.dat", filename_base);
  files->file_outparticleprops =  fopen(files->filename_outparticleprops, "w");

  /* [OUTPUT] File outputting grid properties */
  sprintf(files->filename_outgridprops, "%s_outgridprops.dat", filename_base);
  files->file_outgridprops =  fopen(files->filename_outgridprops, "w");

  return;
} // end OpenFiles()

void CloseFiles(struct Files* files)
/**********************************************************************

        Close files.

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  fclose(files->file_outparticleprops);
  fclose(files->file_outgridprops);

  return;
} // end CloseFiles()
