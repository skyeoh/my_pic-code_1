
#include "main.h"

void OutputParticlePropsTecplot(int it, double time,
                         	struct Files* files,
                         	struct SimlParameters simlparameters,
                         	struct SamplParameters samplparameters,
                         	struct Species* species, struct Particle** particle)
/**********************************************************************

        Output particle properties in the tecplot format. 
	< Be more descriptive!>

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  int is, ipart;

  struct Particle* particlePtr;

  /* Set up zone header */
  fprintf(files->file_outparticleprops, "Zone T = \"Sample at timestep %d\"\n", it);

  for (is = 0; is < simlparameters.Nsp; is++) {
    for (ipart = 0; ipart < species[is].Np; ipart++) {

      particlePtr = particle[is] + ipart; // increment particle pointer

      fprintf(files->file_outparticleprops, "%g %g %g", time, time+simlparameters.dt, time+0.5*simlparameters.dt);
      fprintf(files->file_outparticleprops, " %g %g %g", particlePtr->pos[0], particlePtr->pos[1], particlePtr->pos[2]);
      fprintf(files->file_outparticleprops, " %g %g %g", particlePtr->vel[0], particlePtr->vel[1], particlePtr->vel[2]);
      fprintf(files->file_outparticleprops, " %g %g", particlePtr->mom, particlePtr->KE);
      fprintf(files->file_outparticleprops, " %d", is);
      fprintf(files->file_outparticleprops, "\n");

    } // end loop over particles
  } // end loop over species

  return;
} // end OutputParticlePropsTecplot()

void OutputGridPropsTecplot(int it, double time, 
			    struct Files* files, 
			    struct SimlParameters simlparameters,
			    struct SamplParameters samplparameters,
			    struct Grid* grid)
/**********************************************************************

        Output grid properties in the tecplot format. 
        < Be more descriptive!>

        INPUTS:
        OUTPUTS:

 **********************************************************************/
{
  int igrid, Ngridtotal;

  struct Grid* gridPtr;

  /* Calculate total no. of grid points */
  Ngridtotal = (simlparameters.Ngrid[0])*(simlparameters.Ngrid[1])*(simlparameters.Ngrid[2]); 

  /* Set up zone header */
  fprintf(files->file_outgridprops, "Zone T = \"Sample at timestep %d\"", it);
  fprintf(files->file_outgridprops, " F=POINT");
  fprintf(files->file_outgridprops, " I=%d J=%d K=%d\n", simlparameters.Ngrid[0], simlparameters.Ngrid[1], simlparameters.Ngrid[2]); 

  for (igrid = 0; igrid < Ngridtotal; igrid++) {

    gridPtr = grid + igrid; // increment grid pointer

    fprintf(files->file_outgridprops, "%g", time);
    fprintf(files->file_outgridprops, " %g %g %g", gridPtr->pos[0], gridPtr->pos[1], gridPtr->pos[2]);
    fprintf(files->file_outgridprops, " %g %g", gridPtr->qdens, gridPtr->phi);
    fprintf(files->file_outgridprops, " %g %g %g", gridPtr->Efield[0], gridPtr->Efield[1], gridPtr->Efield[2]);
    fprintf(files->file_outgridprops, " %g", gridPtr->ESE);
    fprintf(files->file_outgridprops, "\n");

  } // end loop over grid points

  return;
} // end OutputGridPropsTecplot()
