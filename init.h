
void GetSimlParameters(struct SimlParameters* simlparameters);
void GetSamplParameters(struct SamplParameters* samplparameters);
void GetSpeciesProps(struct SimlParameters simlparameters, struct Species* species);
void InitializeParticlePos(struct SimlParameters simlparameters,
                           struct Species* species, struct Grid* grid,
			   struct Particle** particle);
void InitializeParticleVel(struct SimlParameters simlparameters,
                           struct Species* species, struct Particle** particle);
void InitializeGrid(struct SimlParameters simlparameters,
                    struct Species* species, struct Grid* grid,
                    struct Particle** particle);
