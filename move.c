
#include "main.h"
#include "move.h"

void MoveParticles(struct Particle* particle, struct Species species, 
		   double* Efield, double dt)
/**********************************************************************

	Move particles. < Be more descriptive!>

	INPUTS:
	OUTPUTS:

 **********************************************************************/
{
  UpdateParticleVel(particle, species, species.qdtoverm, Efield);
  UpdateParticlePos(particle, dt);

  return;
} // end MoveParticles()

void UpdateParticleVel(struct Particle* particle, struct Species species, 
		       double qdtoverm, double* Efield)
/**********************************************************************

	Update particle velocities. < Be more descriptive!>

	INPUTS:
	OUTPUTS:

 **********************************************************************/
{
  double halfmass;   	// half the particle mass

  double oldvel[3];	// velocity before update
  double oldspeedsq;	// square of speed before update
  double newspeedsq;	// square of speed after update

  /* Calculate half the particle mass */
  halfmass = 0.5*species.mass;

  /* Store velocity before update */
  oldvel[0] = particle->vel[0];
  oldvel[1] = particle->vel[1];
  oldvel[2] = particle->vel[2];

  /* Update velocity */
  particle->vel[0]+=qdtoverm*Efield[0];
  particle->vel[1]+=qdtoverm*Efield[1];
  particle->vel[2]+=qdtoverm*Efield[2];

  /* Calculate the square of speed before update */
  oldspeedsq = oldvel[0]*oldvel[0] + oldvel[1]*oldvel[1] + oldvel[2]*oldvel[2];

  /* Calculate the square of speed after update */
  newspeedsq = (particle->vel[0])*(particle->vel[0]) + (particle->vel[1])*(particle->vel[1]) + (particle->vel[2])*(particle->vel[2]);
  
  /* Update kinetic energy */
  particle->KE = halfmass*sqrt(oldspeedsq*newspeedsq);

  // Momentum has not been updated!

  return;
} // end UpdateParticleVel()

void UpdateParticlePos(struct Particle* particle, double dt)
/**********************************************************************

	Update particle positions. < Be more descriptive!>

	INPUTS:
	OUTPUTS:

 **********************************************************************/
{
  /* Update position */
  (particle->pos[0])+=(particle->vel[0])*dt;
  (particle->pos[1])+=(particle->vel[1])*dt;
  (particle->pos[2])+=(particle->vel[2])*dt;

  return;
} // end UpdateParticlePos()
