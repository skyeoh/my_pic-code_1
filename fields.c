
#include "main.h"
#include "fields.h"

void EvaluateFields(struct Particle* particle, double time, double* Efield)
/**********************************************************************

	Evaluate fields at the current particle position.

	INPUTS:
	OUTPUTS:

 **********************************************************************/
{
  CalculateEField(particle, time, Efield);

  return;
} // end EvaluateFields()

void CalculateEField(struct Particle* particle, double time, double* Efield)
/**********************************************************************

	Evaluate the electric field at the current particle 
	position.

	INPUTS:
	OUTPUTS: None.

 **********************************************************************/
{
  double A0, B0, C0; // constants for E-field expression
  double A1, B1, C1;
  double A2, B2, C2;

  /*
   * WARNING: For now, this function has been hardcoded to only do
   *	      a linear expression in space and time. 
   *	      The plan is to only call functions here and their routines will be coded elsewhere.
   *	      Each function will contain a different expression for the E-field (polynomial, exponential, etc.).
   *	      Note that the use of this function may be discontinued
   *          after the interpolation/weighting schemes are coded.
   *	      However, it may be useful in the future. Who knows.
   */

  /* Set constants */
  A0 = 1.0; A1 = 0.0; A2 = 0.0; // V/m
  B0 = 0.0; B1 = 0.0; B2 = 0.0; // V/m^2
  C0 = 0.0; C1 = 0.0; C2 = 0.0; // V/m-s

  /* Set E-field as a linear function of time and particle position */
  Efield[0] = A0 + B0*(particle->pos[0]) + C0*time;
  Efield[1] = A1 + B1*(particle->pos[1]) + C1*time;
  Efield[2] = A2 + B2*(particle->pos[2]) + C2*time;

  return;
} // end CalculateEField()

void AssignEFieldToGrid(struct SimlParameters simlparameters, struct Grid* grid)
/**********************************************************************

        Assign E-field to grid based on charge density on grid.

        INPUTS:
        OUTPUTS: None.

 **********************************************************************/
{
  int igrid; 	  // loop variable
  int Ngridtotal; // total no. of grid points

  double EfieldFactor[3]; // multiplication factor for each component of E-field

  /* Assign and calculate variables */
  Ngridtotal = (simlparameters.Ngrid[0])*(simlparameters.Ngrid[1])*(simlparameters.Ngrid[2]);  

  EfieldFactor[0] = 100.0;
  EfieldFactor[1] = 200.0;
  EfieldFactor[2] = 300.0;

  for (igrid = 0; igrid < Ngridtotal; igrid++) {

    grid[igrid].Efield[0] = EfieldFactor[0]*grid[igrid].qdens;
    grid[igrid].Efield[1] = EfieldFactor[1]*grid[igrid].qdens;
    grid[igrid].Efield[2] = EfieldFactor[2]*grid[igrid].qdens;

  } // end loop over grid points

  return;
} // end AssignEFieldToGrid()
